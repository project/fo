msgid ""
msgstr ""
"Project-Id-Version: Drupal 5.0\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2007-12-29 19:07-0000\n"
"Last-Translator: Lis Gøthe í Jákupsstovu <morshus@morshus.com>\n"
"Language-Team: Faroese <morshus@morshus.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-Language: Faroese\n"
"X-Poedit-Country: FAROE ISLANDS\n"

#: includes/common.inc:324
msgid "Site off-line"
msgstr "Heimasíða offline"

#: includes/common.inc:353
msgid "Page not found"
msgstr "Síða ikki funnin"

#: includes/common.inc:382
msgid "Access denied"
msgstr "Atgongd noktað"

#: includes/common.inc:383
msgid "You are not authorized to access this page."
msgstr "Tú hevur ikki heimild at síggja hesa síðu."

#: includes/common.inc:555
msgid "%message in %file on line %line."
msgstr "%message í %file á reglu %line."

#: includes/common.inc:997
msgid "KB"
msgstr "KB"

#: includes/common.inc:1002
msgid "@size @suffix"
msgstr "@size @suffix"

#: includes/common.inc:1031
#, fuzzy
msgid "0 sec"
msgstr "0 sek"

#: includes/common.inc:1992
msgid "Cron has been running for more than an hour and is most likely stuck."
msgstr "Cron koyringin hevur nú koyrt í meira enn ein tíma, og er tí mest sannlíkt fastløst."

#: includes/common.inc:1999
msgid "Attempting to re-run cron while it is already running."
msgstr "Roynir at koyra cron koyringina umaftur, meðan hon longu koyrir."

#: includes/common.inc:2014
msgid "Cron run completed."
msgstr "Cron koyring fullførd."

#: includes/common.inc:2030
msgid "Cron run exceeded the time limit and was aborted."
msgstr "Cron koyringin fór út um tíðarfreistina, og varð tí brotin av."

#: includes/common.inc:335
msgid "page not found"
msgstr "síða ikki funnin"

#: includes/common.inc:364
msgid "access denied"
msgstr "atgongd noktað"

#: includes/common.inc:1992
#: ;1999;2014;2030
#, fuzzy
msgid "cron"
msgstr "cron"

#: includes/common.inc:993
msgid "1 byte"
msgid_plural "@count bytes"
msgstr[0] "1 být"
msgstr[1] "@count být"

#: includes/common.inc:0
msgid "1 year"
msgid_plural "@count years"
msgstr[0] "1 ár"
msgstr[1] "@count ár"

#: includes/common.inc:0
msgid "1 week"
msgid_plural "@count weeks"
msgstr[0] "1 vika"
msgstr[1] "@count vikur"

#: includes/common.inc:0
msgid "1 day"
msgid_plural "@count days"
msgstr[0] "1 dagur"
msgstr[1] "@count dagar"

#: includes/common.inc:0
msgid "1 hour"
msgid_plural "@count hours"
msgstr[0] "1 tími"
msgstr[1] "@count tímar"

#: includes/common.inc:0
msgid "1 min"
msgid_plural "@count min"
msgstr[0] "1 min"
msgstr[1] "@count min"

#: includes/common.inc:0
msgid "1 sec"
msgid_plural "@count sec"
msgstr[0] "1 sek"
msgstr[1] "@count sek"

